"""file is used to register the model"""
from django.contrib import admin

# Register your models here.
from customer.models import Role, AccountTransaction, CustomerAccount, MyUser


class MyUserAdmin(admin.ModelAdmin):
    """
    Custom Model admin for User Model.
    """
    list_display = ("name", "email")


class CustomerAccountAdmin(admin.ModelAdmin):
    """
    Custom Model admin for Customer Account .
    """
    list_display = ("account_number", "user")


class RoleAdmin(admin.ModelAdmin):
    """
    Custom Admin for Role admin.
    """
    list_display = ("name",)


class AccountTransactionAdmin(admin.ModelAdmin):
    """
    Custom Model admin for Customer Account .
    """
    list_display = ("transaction_type", "customer_account")


admin.site.register(Role, RoleAdmin)
admin.site.register(AccountTransaction, AccountTransactionAdmin)
admin.site.register(CustomerAccount, CustomerAccountAdmin)
admin.site.register(MyUser, MyUserAdmin)
