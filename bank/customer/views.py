"""File is used to write the view"""


# django import
import csv
from logging import exception
from smtplib import SMTPException

from celery import shared_task
from django.core.mail import EmailMultiAlternatives
from django.db.models import Q
from django.http import HttpResponse
from django.template.loader import render_to_string
from rest_framework import status
from rest_framework.mixins import (CreateModelMixin, RetrieveModelMixin, UpdateModelMixin, DestroyModelMixin,
                                   ListModelMixin)
from rest_framework.response import Response
from rest_framework.viewsets import GenericViewSet
from customer.models import MyUser, CustomerAccount, AccountTransaction, DEPOSIT, WITHDRAW

message = "Please enter the amount"


class CustomModelViewSet(CreateModelMixin,
                         RetrieveModelMixin,
                         UpdateModelMixin,
                         DestroyModelMixin,
                         ListModelMixin,
                         GenericViewSet):
    """
    A view-set that provides default `create()`, `retrieve()`, `update()`,
    `partial_update()`, `destroy()` and `list()` actions, and provide a unique response format .
    """
    pass


def custom_response(status, detail, data=None, **kwargs):
    """
    function is used for getting same global response for all api
    :param detail: success message
    :param data: data
    :param status: http status
    :return: Json response
    """
    if "error_type" in kwargs:
        return Response({"data": data, "detail": detail, "error_type": kwargs.get("error_type")}, status=status)
    return Response({"data": data, "detail": detail}, status=status, **kwargs)


def custom_error_response(status, detail, **kwargs):
    """
    function is used for getting same global error response for all api
    :param detail: error message .
    :param status: http status.
    :return: Json response
    """
    if not detail:
        detail = {}
    return Response({"detail": detail}, status=status, **kwargs)


class DepositAmountView(CustomModelViewSet):
    """
    ViewSet is used to add bar-i manager.
    """
    serializer_class = None
    http_method_names = ['post', ]

    def create(self, request, *args, **kwargs):
        """
        Method is used to add deposite
        """
        amount = self.request.data.get('amount')
        date = self.request.data.get('date')
        user = MyUser.objects.filter(email='user01@yopmail.com').last()
        if amount:
            user_account_balance = CustomerAccount.objects.filter(user=user)
            AccountTransaction.objects.create(transaction_type=DEPOSIT, customer_account=user_account_balance.last(),
                                              date=date)
            deposit = user_account_balance.last().account_balance + float(amount)
            user_account_balance.update(account_balance=deposit)
            return custom_response(status=status.HTTP_201_CREATED,
                                   detail="Money Deposit successfully.")
        return custom_error_response(status=status.HTTP_400_BAD_REQUEST,
                                     detail=message)


def send_withdraw_email(data):
    """
    used to send mail from everywhere in the project
    """
    template = 'transaction.html'
    html_message = render_to_string(template, context=data)
    sending_mail(to_user=data["to_user"], cc_user=data["cc_user"],
                 html_message=html_message, attachment=None, subject=data["subject"])


class WithdrawAmountView(CustomModelViewSet):
    """
    ViewSet is used to add withdraw the amount.
    """
    serializer_class = None
    http_method_names = ['post', ]

    def create(self, request, *args, **kwargs):
        """
        Method is used to add client
        """
        amount = self.request.data.get('amount')
        date = self.request.data.get('date')
        user = MyUser.objects.filter(email='user01@yopmail.com').last()
        if amount:
            user_account_balance = CustomerAccount.objects.filter(user=user)
            AccountTransaction.objects.create(transaction_type=WITHDRAW, customer_account=user_account_balance.last(),
                                              date=date)
            withdraw = user_account_balance.last().account_balance - float(amount)
            user_account_balance.update(account_balance=withdraw)
            data = {
                "subject": "Transaction History",
                'to_user': [user.email],
                'cc_user': ['snigdha2012sharma@gmail.com']
            }
            send_withdraw_email(data)
            return custom_response(status=status.HTTP_201_CREATED,
                                   detail="Money withdraw successfully.")
        return custom_error_response(status=status.HTTP_400_BAD_REQUEST, detail=message)


class GetAmountView(CustomModelViewSet):
    """
    ViewSet is used to get amount.
    """
    serializer_class = None
    http_method_names = ['post', ]

    def create(self, request, *args, **kwargs):
        """
        Method is used to add client
        """
        account_no = self.request.data.get('account_no')
        user = MyUser.objects.filter(email='user01@yopmail.com').last()
        if account_no:
            amount = AccountTransaction.objects.filter(Q(customer_account__user=user) |
                                                       Q(customer_account__account_number=account_no)).last()
            data = {
                'account_balance': amount.customer_account.account_balance
            }
            return custom_response(status=status.HTTP_201_CREATED, data=data,
                                   detail="Your account balance")
        return custom_error_response(status=status.HTTP_400_BAD_REQUEST, detail=message)


def generate_csv(amount):
    """
    Generating csv of user data
    """
    csv_file = HttpResponse(content_type='text/csv')
    writer = csv.writer(csv_file)
    if amount.exists():
        writer.writerow(['Account_number', 'Account_balance', 'transaction_type'])
    for instance in amount:
        writer.writerow(
            [instance.customer_account.account_number, instance.customer_account.account_balance,
             instance.transaction_type])
    return csv_file


def generate_email_list_csv_task(amount):
    """
    generate and send csv email task
    """
    csv_file = generate_csv(amount)
    user = MyUser.objects.filter(role__name='bank_manager').last()
    data = {
        "subject": "Transaction History",
        "message": "",
        'to_user': [user.email],
        'cc_user': ['snigdha2012sharma@gmail.com'],
        'attachment': {'attachment_name': "Transaction History",
                       'file': csv_file,
                       'file_type': 'text/csv'
                       }
    }
    send_email(data)


def send_email(data):
    """
`   used to send mail from everywhere in the project
    """
    template = 'text.html'
    html_message = render_to_string(template, context=data)
    sending_mail(to_user=data["to_user"], cc_user=data["cc_user"],
                 html_message=html_message, attachment=data["attachment"], subject=data["subject"])


@shared_task()
def sending_mail(to_user, cc_user, message='', html_message=None, attachment=None, subject=None):
    """
    This function is used for sending mail to end-user
    :param subject: mail subject
    :param from_email: mail subject
    :param to_user: end_user email
    :param cc_user: owner email
    :param html_message: email message
    :param attachment: attachment
    :param message: email message
    :return: send mail or failure status
    """
    try:
        email = EmailMultiAlternatives(subject, from_email='snigdha2012sharma@gmail.com', to=to_user, cc=cc_user)
        if html_message:
            email.attach_alternative(html_message, 'text/html')
        if attachment:
            email.attach(attachment['attachment_name'], attachment['file'].getvalue(), attachment['file_type'])
        email.send()
        return True
    except SMTPException as exe:
        exception("Sending Mail Exception : {}".format(str(exe)))
        return False


class TransactionDataView(CustomModelViewSet):
    """
    View is used to get data in excel
    """
    serializer_class = None
    http_method_names = ['get', ]
    queryset = None

    def list(self, request, *args, **kwargs):
        """
        Method is used to add client
        """
        to_date = self.request.GET.get('to_date')
        from_date = self.request.GET.get('from_date')
        user = MyUser.objects.filter(email='user01@yopmail.com').last()
        if to_date and from_date:
            amount = AccountTransaction.objects.filter(Q(customer_account__user=user),
                                                       date__in=[to_date, from_date])
            generate_email_list_csv_task(amount)
            return custom_response(status=status.HTTP_200_OK, detail='ok')
        return custom_error_response(status=status.HTTP_400_BAD_REQUEST, detail=message)
