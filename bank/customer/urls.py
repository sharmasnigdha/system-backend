"""
urls files.
"""

# third party import
from django.urls import path, include

# local imports
from customer.router import OptionalSlashRouter
from customer import views

router = OptionalSlashRouter()

router.register(r'deposit', views.DepositAmountView, basename='deposit')
router.register(r'withdraw', views.WithdrawAmountView, basename='withdraw')
router.register(r'total-balance', views.GetAmountView, basename='total-balance')
router.register(r'transaction-details', views.TransactionDataView, basename='transaction-details')

urlpatterns = [
    path(r'account/', include(router.urls)),
]
