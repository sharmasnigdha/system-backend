"""File is used to register the models"""

# django import
from django.db import models


WITHDRAW = 'withdraw'
DEPOSIT = 'deposit'

TRANSACTION_TYPE = (
    (WITHDRAW, WITHDRAW),
    (DEPOSIT, DEPOSIT)
)


class BaseDateModel(models.Model):
    """
    Common BaseData model for all the models in the project.
    """
    created_at = models.DateTimeField(auto_now_add=True)
    updated_at = models.DateTimeField(auto_now=True)
    is_active = models.BooleanField(default=True)

    class Meta:
        """
        Meta class of BaseDate Model.
        """
        abstract = True


class Role(BaseDateModel):
    """
    A base class to store role.

    name is required field.
    """
    name = models.CharField(max_length=100)

    def __str__(self):
        return self.name


class MyUser(BaseDateModel):
    """Model is used to store user info"""
    name = models.CharField(max_length=250, null=True, blank=True)
    email = models.EmailField(unique=False)
    role = models.ForeignKey(Role, null=True, blank=True, on_delete=models.CASCADE, related_name='user_role')


class CustomerAccount(BaseDateModel):
    """model is used to store the customer account info"""
    account_number = models.CharField(max_length=200, null=True, blank=True)
    account_balance = models.FloatField(default=0.0, null=True, blank=True)
    user = models.ForeignKey(MyUser, null=True, blank=True, on_delete=models.CASCADE, related_name='user_account')


class AccountTransaction(BaseDateModel):
    """Model is used to store transaction info"""
    customer_account = models.ForeignKey(CustomerAccount, null=True, blank=True, on_delete=models.CASCADE)
    date = models.DateTimeField(null=True, blank=True)
    transaction_type = models.CharField(max_length=50, choices=TRANSACTION_TYPE)

