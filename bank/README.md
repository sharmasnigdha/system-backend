Banking System

1. Installing Required tools  

    * **(option 1 - local) :**

        * Install database
            - Ubuntu:-
                * Client:- sudo apt-get install -y postgresql-10 postgresql-contrib-10
     

            - OSX:- brew install postgresql@10

2. Environment setup:

    * Install pip and virtualenv:
        - sudo apt-get install python3-pip
        - pip install --upgrade pip
        - sudo pip3 install virtualenv or sudo pip install virtualenv

    * Create virtual environment:
        - virtualenv env

        OPTIONAL:- In case finding difficulty in creating virtual environment by
                  above command , you can use the following commands too.

            *   Create virtualenv using Python3:-
                    - virtualenv -p python3.5 env
            *   Instead of using virtualenv you can use this command in Python3 for creating virtual environment:-
                    - python3.5 -m venv env

    * Activate environment:
        - source env/bin/activate

    * Clone project:
        - ```
            git clone https://sharmasnigdha@bitbucket.org/sharmasnigdha/system-backend.git
          ```

    * Checkout to branch
        - git checkout "branch_name"

    * Install the requirements(according to server) by using command:
        - cd project/
        * for local system follow below command
        ```
          pip3 install -r requirements.txt or pip install -r requirements.txt
        ```
        
3. Database Setup

     * Create database with proper permissions to the db-user:
         * create user user_name;
         * password user_name;
         * create database db_name;
         * grant all privileges on database db_name to user_name;

     * Add following information in .env file,to get the actual values, please contact to project owner.

          - DB_ENGINE="******"
          - DB_NAME="****"
          - DB_USERNAME="*******"
          - DB_PASSWORD="*******"
          - DB_HOST="*******"
          - DB_PORT=*******
          - DEBUG=*******
          - SECRET_KEY="*******"
          - SETTINGS="*******"

     * DB migrations:
        ```
        $ python manage.py migrate
        ```

4. Run servers:
    ```
     $ python manage.py runserver
    ```
5. Run Fixtures commands for making objects in models.

    * python manage.py loaddata fixtures/account.json
    
    * python manage.py loaddata fixtures/role.json
    
    * python manage.py loaddata fixtures/user.json
    
6. There are 4 api's for this project:
    
    Api-url : http://localhost:8090/api/v1/account/deposit/ 
              request - { "amount":1000, "date": "2021-11-04" }
    Api-url : http://localhost:8090/api/v1/account/withdraw/ 
              request - { "amount":1000, "date": "2021-11-04" }
    Api-url : http://localhost:8090/api/v1/account/total-balance/ 
              request - { "account_no": 120912340981 }
    Api-url : http://localhost:8090/api/v1/account/transaction-details/?to_date=2021-11-04&from_date=2021-10-04
